package kubernetes

import (
		"os"
		"path/filepath"
		log "github.com/sirupsen/logrus"
		"k8s.io/client-go/dynamic"
		"k8s.io/client-go/tools/clientcmd"
)

// K8sConnector function connects with default kubernetes credentials into defined cluster
func K8sConnector() dynamic.Interface {
	// get kubeconfig
	kubeconfig := filepath.Join(
		os.Getenv("HOME"), ".kube", "config")
	log.Info("Attempting to connect to K8s cluster using kubeconfig file: " + kubeconfig)
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		log.Fatal(err)
	}
	// create k8s client
	dynamicClient, err := dynamic.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Client connected!")

	return dynamicClient
}

