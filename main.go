package main

import (
  log "github.com/sirupsen/logrus"
  "fmt"
  "net/url"
  "limani/dispatcher"
  "limani/models"
  "github.com/gofiber/fiber/v2"
)

func queryParser (queryString string) url.Values {
	var paramsObject, e = url.ParseQuery(queryString)
	if e != nil {
		log.Panic(e)
	 }
	 return paramsObject
}

func endpointDispatcher(ctx *fiber.Ctx) error {
	var allParams = queryParser(string(ctx.Request().URI().QueryString()))
	var path = fmt.Sprintf("/%s", ctx.Params("*"))
	var headers = models.Headers(ctx.GetReqHeaders())
	
	requestedEndpoint := models.NewRequestedEndpoint(path, allParams, headers)
	matchedEndpoints := dispatcher.SearchMatching(requestedEndpoint)
	if len(matchedEndpoints) > 0 {
		log.Info("Path matched!")
		log.Info()
		validatedEndpoints := dispatcher.FindValidEndpoints(requestedEndpoint, matchedEndpoints)
		events := dispatcher.EventFactory(validatedEndpoints, requestedEndpoint)
		log.Info(events)

		return ctx.SendString("It's supported path - new object created!")
	  } 
	
	return ctx.SendString("This path doesn't match any of registered endpoints!")
	
	// PSEUDOCODE
	// if matched_endpoints
	// validated_endpoints := Validator.FindValidEndpoints(SearchingEndpoint, matched_endpoints)
	
	// for endpoints in validated_endpoints {
		// prepare events objects from ctx
		// events := EventFactory.NewEvents(endpoint)
		// for event in events {
			// event.render(ctx.params, ctx.payload)
		// }
	// }
	// events.RenderData()
}

func main() {
	app := fiber.New()

	app.Get("/endpoint/*", endpointDispatcher)
	app.Post("/endpoint/*", endpointDispatcher)

	if err := app.Listen(":3000"); err != nil {
		log.Panic(err)
	}
}
