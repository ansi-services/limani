// package dispatcher is interaction and context handler for different high level actions
// module validator is matcher to verify if incoming requests pass requirements of event handler

package dispatcher

import (
		log "github.com/sirupsen/logrus"
		models "limani/models" 
  )
  
  // FindValidEndpoints function validate requirements for matched endpoints and return only this that pass rules
 func FindValidEndpoints(requestedEndpoint models.RequestedEndpoint, matchedEndpoints []models.Endpoint) []models.Endpoint {
	var vends []models.Endpoint
	log.Info("Validating endpoints")
	for _, endpoint := range matchedEndpoints {
		if ValidateEndpointRequirements(endpoint, requestedEndpoint) {
			vends = append(vends, endpoint)
		}
	}
	return vends
 }

 // ValidateEndpointRequirements checks if request match endpoint requirements
 func ValidateEndpointRequirements(endpoint models.Endpoint, requestedEndpoint models.RequestedEndpoint) bool {
	for _, requirement := range endpoint.Requirements {
		if !requirement.Validate(requestedEndpoint) {
			return false
		}
	}
	return true
 }