// package dispatcher is interaction and context handler for different high level actions
// module eventfactory is designed to handle registered events and spark them on user request

package dispatcher

import (
	models "limani/models"
)

// EventFactory is function that dig for events that need to be sparked based on incomming request
func EventFactory (validatedEndpoints []models.Endpoint, requestedEndpoint models.RequestedEndpoint) []models.EventObject {
	var events []models.EventObject
	for _, endpoint := range validatedEndpoints {
		events = append(events, digEvents(endpoint, requestedEndpoint)...)
	}
	return events
}

// digEvents is function that populate event objects from endpoint and prepare final object to render event
func digEvents (endpoint models.Endpoint, requestedEndpoint models.RequestedEndpoint) []models.EventObject {
	var events []models.EventObject
	for _, event := range endpoint.Events {
		ev := models.EventObject { Event: event, Context: requestedEndpoint }

		events = append(events, ev)
	}
	return events
}