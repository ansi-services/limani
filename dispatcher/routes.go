// package dispatcher is interaction and context handler for different high level actions
// module routes is dispatcher to handle user requests and match with registered endpoints

package dispatcher

import (
        log "github.com/sirupsen/logrus"
        models "limani/models"
        utils "limani/utils"
)

var endpoints []models.Endpoint

func init() {
        dynamicClient := utils.K8sConnector()
        for _, endpoint := range models.FetchEndpoints(dynamicClient) {
                register(endpoint)
        }
}

// SearchMatching function looks for endpoints that match registered once in Kubernetes
func SearchMatching(requestedEndpoint models.RequestedEndpoint) []models.Endpoint {
        displayRegisteredEndpoints()

        log.Info("Looking for path: ", requestedEndpoint.Path)
        return MatchRequestedEndpoint(requestedEndpoint, endpoints)
}

func register(e models.Endpoint) {
        log.Info("Registering new endpoint ", e.Path)
        endpoints = append(endpoints, e)
}

func displayRegisteredEndpoints() {
        log.Info("Registered endpoints")
        for _, endpoint := range endpoints {
                log.Info("Endpoint path: ", endpoint.Path)
        }
}

// MatchRequestedEndpoint function looks for registered endpoints that match path in incomming requests
func MatchRequestedEndpoint(requestedEndpoint models.RequestedEndpoint, endpoints []models.Endpoint) []models.Endpoint {
	var resultEndpoints []models.Endpoint
	log.Debug("Searched path: ", requestedEndpoint.Path)
	for _, endpoint := range endpoints {
		log.Debug("Considered path: ", endpoint.Path)
		if endpoint.Match(requestedEndpoint.Path) {
			log.Debug("Endpoint found: ", endpoint)
			resultEndpoints = append(resultEndpoints, endpoint)
		}
	}
	if len(resultEndpoints) == 0 {
		log.Debug("Not Endpoint's matching searched path!")
	}
	return resultEndpoints
}