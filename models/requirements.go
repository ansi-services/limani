package models

import (
	"fmt"
	"net/url"
	log "github.com/sirupsen/logrus"
)

// Requirement structure contains all data described in definition of requirement, if value is empty then any value should be accepted
type Requirement struct {
	Type string
	Field string
	Value string
}

// String to print requirement data in organized form
func (r Requirement) String() string {
	if r.Value != "" {
		return fmt.Sprintf("Requirement for %s: %s with value: %s.", r.Type, r.Field, r.Value)
	} else {
		return fmt.Sprintf("Requirement for %s: %s without explicit value.", r.Type, r.Field)
	}
}

func (requirement Requirement) getValues (requestedEndpoint RequestedEndpoint) Map {
	if (requirement.Type == "param") {
		return requestedEndpoint.Params
	}
	if (requirement.Type == "header") {
		return requestedEndpoint.Headers
	}
	return url.Values{}
}

// Validate checks if requested endpoint matches requirement
func (requirement Requirement) Validate (requestedEndpoint RequestedEndpoint) bool {
	var data = requirement.getValues(requestedEndpoint)
	if data.Has(requirement.Field) {
		if requirement.Value != "" {
			if data.Get(requirement.Field) == requirement.Value {
				log.Info("Requirement for ", requirement.Type,": ", requirement.Field, " with value: ", requirement.Value, " passed!")
				return true
			}
		} else {
			log.Info("Requirement for ", requirement.Type,": ", requirement.Field, " without explicit value passed!")
			return true
		}
	}
	log.Info("Requirement for ", requirement.Type,": ", requirement.Field, " doesn't pass!")
	return false
}
