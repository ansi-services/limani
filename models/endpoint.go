// Package models contains set of data models to store retrived from user and kubernetes informations
// model for Endpoint is used to store and aggregate data about registered endpoints that should be handled
// model for RequestedEndpoint is used to store information about user request

package models

import (
	"fmt"
	"context"
	"net/url"
	log "github.com/sirupsen/logrus"

	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var endpointSchema = schema.GroupVersionResource{
	Group:    "limani.ansi.services",
	Version:  "v1",
	Resource: "endpoints",
}

// Endpoint structure defining registered endpoint
type Endpoint struct {
	Path string
	Requirements []Requirement
	Events []Event
}

// RequestedEndpoint structure defining what endpoint user is requesting with API
type RequestedEndpoint struct {
	Endpoint
	Params url.Values
	Headers Headers
}

// NewRequestedEndpoint is a function that construct requested endpoint object
func NewRequestedEndpoint(path string, params url.Values, headers Headers) RequestedEndpoint {
	return RequestedEndpoint{Endpoint: Endpoint{Path: path}, Params: params, Headers: headers}
}

// Match function compare provided path with endpoint specification
func (e Endpoint) Match (p string) bool {
	return e.Path == p
}

// String to display endpoint data
func (e Endpoint) String() string {
	return fmt.Sprintf("Endpoint registered for path: %s with Requirements: %s", e.Path, e.Requirements)
}

// FetchEndpoints function gets endpoints definitions from Kubernetes and load into Endpoint structure
func FetchEndpoints(dynamicClient dynamic.Interface) []Endpoint {
	log.Info("Fetching Endpoints definitions from Kubernetes ...")
	endpoints, err := dynamicClient.Resource(endpointSchema).Namespace("default").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			log.Info("failed to fetch Endpoints!")
			return nil
		}

		var ends []Endpoint

		if len(endpoints.Items) > 0 {
			for _, endpoint := range endpoints.Items {
				log.Info("Endpoint: ", endpoint.GetName(), " found!")
				path := fmt.Sprintf("%s", endpoint.Object["spec"].(map[string]interface{})["path"])

				spec := endpoint.Object["spec"].(map[string]interface{})
				
				var reqs []Requirement
				if (spec["requirements"] != nil){
					requirements := spec["requirements"].([]interface{})
					reqs = getRequirements(requirements)
				}
				
				var evs []Event
				if (spec["events"] != nil){
					events := spec["events"].([]interface{})
					evs = getEvents(events)
				}

				log.Debug("Extracted endpoint path: ", path)
				ends = append(ends, Endpoint{ Path: path, Requirements: reqs, Events: evs })
			}
		} else {
			log.Info("No endpoints found!")
		}

	return ends
}

func getRequirements(requirements []interface{}) []Requirement {
	var reqs []Requirement

	for _, req := range requirements {
		data := req.(map[string]interface{})
		log.Info("Requirement type of ",data["type"], " on field: ", data["field"], " with value: ", data["value"])
		
		var value string
		if data["value"] == nil {
			value = ""
		} else {
			value = fmt.Sprintf("%s", data["value"])
		}

		reqObj := Requirement {
			Type: fmt.Sprintf("%s", data["type"]), 
			Field: fmt.Sprintf("%s", data["field"]), 
			Value: value}
		reqs = append(reqs, reqObj)
	}
	return reqs
}

func getEvents(events []interface{}) []Event {
	var evs []Event

	for _, event := range events {
		data := event.(map[string]interface{})

		eventType := data["type"]
		
		object := data["object"].(map[string]interface{})
		objectKind := object["kind"]
		objectAPIVersion := object["apiVersion"]
		objectName := object["name"]
		objectSpec := object["spec"]

		log.Info("Event Object to be ", eventType, "d: ", objectKind, "[", objectAPIVersion, "] named ", objectName, " with spec: ", objectSpec, ".")

		k8sObject := K8sObject {
			Kind: fmt.Sprintf("%s", objectKind),
			APIVersion: fmt.Sprintf("%s", objectAPIVersion),
			Name: fmt.Sprintf("%s", objectName),
			Spec: objectSpec}

		eventObject := Event {
			Type: fmt.Sprintf("%s", eventType),
			Object: k8sObject}

		evs = append(evs, eventObject)
	}

	return evs
}