package models

type Headers map[string]string

func (h Headers) Has(key string) bool {
	_, ok := h[key]
	return ok
}

func (h Headers) Get(key string) string {
	if h == nil {
		return ""
	}
	return h[key]
}

type Map interface {
	Has(string) bool
	Get(string) string
}