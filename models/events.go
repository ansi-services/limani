// Package models contains set of data models to store retrived from user and kubernetes informations
// model Events is used to store informations about event that should be generated in Kubernetes on request
package models

import (
	"fmt"
)

// K8sObject is structure to save object that will be result of event
type K8sObject struct {
	Kind string
	APIVersion string
	Name string
	Spec interface{}
}

// Event structure is to save event information from Kubernetes, it contain type and object for K8s
type Event struct {
	Type string
	Object K8sObject
}

// String to print event data in organized form
func (e Event) String() string {
	return fmt.Sprintf("Event to %s object: %s[%s] named %s.", e.Type, e.Object.Kind, e.Object.APIVersion, e.Object.Name)
}

// EventObject is structure to handle defined event and request context to render event and send into kubernetes cluster
type EventObject struct {
	Event Event
	Context RequestedEndpoint
}

// RenderWithData is function to render EventObject
func (e EventObject) RenderWithData() {

}
